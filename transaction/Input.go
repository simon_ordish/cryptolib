package transaction

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"

	"bitbucket.org/simon_ordish/cryptolib"
)

/*
Field	                     Description                                                   Size
--------------------------------------------------------------------------------------------------------
Previous Transaction hash  doubled SHA256-hashed of a (previous) to-be-used transaction	 32 bytes
Previous Txout-index       non negative integer indexing an output of the to-be-used      4 bytes
                           transaction
Txin-script length         non negative integer VI = VarInt                               1-9 bytes
Txin-script / scriptSig	   Script	                                                        <in-script length>-many bytes
sequence_no	               normally 0xFFFFFFFF; irrelevant unless transaction's           4 bytes
                           lock_time is > 0
*/

// Input is a representation of a transaction input
type Input struct {
	PreviousTxHash     [32]byte
	PreviousTxOutIndex uint32
	PreviousTxSatoshis uint64
	PreviousTxScript   *Script
	SigScript          *Script
	SequenceNumber     uint32
}

// NewInput creates a new Input object with a finalised sequence number.
func NewInput() *Input {
	b := make([]byte, 0)
	s := NewScriptFromBytes(b)

	return &Input{
		SigScript:      s,
		SequenceNumber: 0xFFFFFFFF,
	}
}

// NewInputFromBytes returns a transaction input from the bytes provided.
func NewInputFromBytes(bytes []byte) (*Input, int) {
	i := Input{}

	copy(i.PreviousTxHash[:], cryptolib.ReverseBytes(bytes[0:32]))

	i.PreviousTxOutIndex = binary.LittleEndian.Uint32(bytes[32:36])

	offset := 36
	l, size := cryptolib.DecodeVarInt(bytes[offset:])
	offset += size

	i.SigScript = NewScriptFromBytes(bytes[offset : offset+int(l)])

	i.SequenceNumber = binary.LittleEndian.Uint32(bytes[offset+int(l):])

	return &i, offset + int(l) + 4
}

func NewInputFromReader(r *bufio.Reader) (*Input, []byte, error) {
	i := Input{}

	bytes := make([]byte, 0)

	previousTxHash := make([]byte, 32)
	if n, err := io.ReadFull(r, previousTxHash); n != 32 || err != nil {
		return nil, nil, fmt.Errorf("Could not read previousTxHash(32), got %d bytes and err: %v", n, err)
	}
	bytes = append(bytes, previousTxHash...)

	copy(i.PreviousTxHash[:], cryptolib.ReverseBytes(previousTxHash))

	prevIndex := make([]byte, 4)
	if n, err := io.ReadFull(r, prevIndex); n != 4 || err != nil {
		return nil, nil, fmt.Errorf("Could not read prevIndex(4), got %d bytes and err: %v", n, err)
	}
	bytes = append(bytes, prevIndex...)
	i.PreviousTxOutIndex = binary.LittleEndian.Uint32(prevIndex)

	l, b, err := cryptolib.DecodeVarIntFromReader(r)
	if err != nil {
		return nil, nil, fmt.Errorf("Could not read varint: %v", err)
	}
	bytes = append(bytes, b...)

	script := make([]byte, l)
	if n, err := io.ReadFull(r, script); uint64(n) != l || err != nil {
		return nil, nil, fmt.Errorf("Could not read script(%d), got %d bytes and err: %v", l, n, err)
	}
	bytes = append(bytes, script...)
	i.SigScript = NewScriptFromBytes(script)

	sequence := make([]byte, 4)
	if n, err := io.ReadFull(r, sequence); n != 4 || err != nil {
		return nil, nil, fmt.Errorf("Could not read sequence(4), got %d bytes and err: %v", n, err)
	}
	bytes = append(bytes, sequence...)
	i.SequenceNumber = binary.LittleEndian.Uint32(sequence)

	return &i, bytes, nil
}

func (i *Input) String() string {
	return fmt.Sprintf(`prevTxHash:   %x
prevOutIndex: %d
scriptLen:    %d
script:       %x
sequence:     %x
`, i.PreviousTxHash, i.PreviousTxOutIndex, len(*i.SigScript), i.SigScript, i.SequenceNumber)
}

// Hex encodes the Input into a hex byte array.
func (i *Input) Hex(clear bool) []byte {
	hex := make([]byte, 0)

	hex = append(hex, cryptolib.ReverseBytes(i.PreviousTxHash[:])...)
	hex = append(hex, cryptolib.GetLittleEndianBytes(i.PreviousTxOutIndex, 4)...)
	if clear {
		hex = append(hex, 0x00)
	} else {
		if i.SigScript == nil {
			hex = append(hex, cryptolib.VarInt(0)...)
		} else {
			hex = append(hex, cryptolib.VarInt(uint64(len(*i.SigScript)))...)
			hex = append(hex, *i.SigScript...)
		}
	}
	hex = append(hex, cryptolib.GetLittleEndianBytes(i.SequenceNumber, 4)...)

	return hex
}
