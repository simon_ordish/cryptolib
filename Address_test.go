package cryptolib

import (
	"encoding/hex"
	"fmt"
	"testing"

	"github.com/bitcoinsv/bsvd/bsvec"
	"github.com/bitcoinsv/bsvd/chaincfg"
	"github.com/bitcoinsv/bsvutil"
)

func TestAddressToPubKeyHash(t *testing.T) {
	publicKeyhash := "8fe80c75c9560e8b56ed64ea3c26e18d2c52211b"

	addressTestnet := "mtdruWYVEV1wz5yL7GvpBj4MgifCB7yhPd"
	addr, err := NewAddressFromString(addressTestnet)
	expectedPublicKeyhashTestnet := addr.PublicKeyHash

	addressLive := "1E7ucTTWRTahCyViPhxSMor2pj4VGQdFMr"
	addr2, err := NewAddressFromString(addressLive)
	expectedPublicKeyhashLivenet := addr2.PublicKeyHash

	if err != nil {
		t.Error(err)
	}

	if publicKeyhash != expectedPublicKeyhashTestnet {
		t.Errorf("PKH from testnet address %s incorrect,\ngot: %s\nexpected: %s", addressTestnet, publicKeyhash, expectedPublicKeyhashTestnet)
	}

	if publicKeyhash != expectedPublicKeyhashLivenet {
		t.Errorf("PKH from Live address %s incorrect,\ngot: %s\nexpected: %s", addressLive, publicKeyhash, expectedPublicKeyhashTestnet)
	}
}

func TestGenesisAddress(t *testing.T) {

	publicKey := "04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f"

	defaultNet := &chaincfg.MainNetParams

	h, _ := hex.DecodeString(publicKey)
	pk, _ := bsvec.ParsePubKey(h, bsvec.S256())
	t.Logf("x:%s y:%s", pk.X, pk.Y)

	addr, err := bsvutil.DecodeAddress(publicKey, defaultNet)
	if err != nil {
		fmt.Println(err)
		return
	}

	addressLive := addr.EncodeAddress()

	expectedAddressLive := "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa"

	if addressLive != expectedAddressLive {
		t.Errorf("Address Live from public key  %x incorrect,\ngot: %s\nexpected: %s", publicKey, addressLive, expectedAddressLive)
	}
}

func TestPublicKeyHashFromPublicKeyStr(t *testing.T) {
	pubKey := "03630019a270db9f09ba635bccee980a0b96e19d89533c6a9be26e5f6282ccc47a"
	expectedPublicKeyhash := "05a23cf9b42a0ccb5cf2b2bcb70bd3ac0d2c9852"

	publicKeyHash, err := PublicKeyHashFromPublicKeyStr(pubKey)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if expectedPublicKeyhash != publicKeyHash {
		t.Logf("Expected %q, got %q", expectedPublicKeyhash, publicKeyHash)
		t.FailNow()
	}
}

func TestPublicKeyToAddress(t *testing.T) {
	publicKey := "0285e9737a74c30a873f74df05124f2aa6f53042c2fc0a130d6cbd7d16b944b004"
	// pkh := "9cf8b938ce2b14f68c59d7ed166b2ae242198037"

	expectedAddressTestnet := "mupwfbLpEposb7h4E8WyxCWbt5UYMHcV27"
	addr, err := NewAddressFromPublicKey(publicKey, false)
	addressTestnet := addr.AddressString

	expectedAddressLive := "1FJzNYFqRoNcp1DSWZYc8HJH25sqPgmyw3"
	addr2, err := NewAddressFromPublicKey(publicKey, true)
	addressLive := addr2.AddressString

	if err != nil {
		t.Error(err)
	}

	if addressTestnet != expectedAddressTestnet {
		t.Errorf("Address Testnet from public key address %s incorrect,\ngot: %s\nexpected: %s", publicKey, addressTestnet, expectedAddressTestnet)
	}

	if addressLive != expectedAddressLive {
		t.Errorf("Address Live from public key  %s incorrect,\ngot: %s\nexpected: %s", publicKey, addressLive, expectedAddressLive)
	}
}
