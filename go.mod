module bitbucket.org/simon_ordish/cryptolib

go 1.17

require (
	github.com/bitcoinsv/bsvd v0.0.0-20190609155523-4c29707f7173
	github.com/bitcoinsv/bsvutil v0.0.0-20181216182056-1d77cf353ea9
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
)

require github.com/davecgh/go-spew v1.1.1 // indirect
